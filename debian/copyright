Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: AtteanX::Store::MemoryTripleStore
Upstream-Contact: https://github.com/kasei/atteanx-store-memorytriplestore/issues
Source: https://metacpan.org/release/AtteanX-Store-MemoryTripleStore
 https://github.com/kasei/atteanx-store-memorytriplestore

Files: *
Copyright:
  © 2015, Gregory Todd Williams <gwilliams@cpan.org>
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
License: GPL-2+

Files: avl.*
Copyright:
  © 1998-2002,2004, Free Software Foundation, Inc.
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
License: GPL-2+

Files: linenoise.*
Copyright:
  © 2010-2013, Salvatore Sanfilippo <antirez@gmail.com>
  © 2010-2013, Pieter Noordhuis <pcnoordhuis@gmail.com>
License: BSD-2-Clause

Files: debian/*
Copyright:
  © 2015, Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3, or (at your option) any
 later version.
License: GPL-3+

License: GPL-2+
License-Reference: /usr/share/common-licenses/GPL-2

License: GPL-3+
License-Reference: /usr/share/common-licenses/GPL-3

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
